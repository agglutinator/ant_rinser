import pyb, time
from machine import I2C
I2C_MINIMOTO_ADDRESS = 0x64

i2c_contoller = I2C(scl='X1', sda='X2', freq=400000)

RINSE_EVERY_TWO_SECONDS = 1000 * 2
RINSE_EVERY_MINUTE = 1000 * 60
RINSE_EVERY_HOUR = RINSE_EVERY_MINUTE * 60
RINSE_TWICE_A_DAY = RINSE_EVERY_HOUR * 12
RINSE_EVERY_DAY = RINSE_EVERY_HOUR * 24

RINSE_PERIOD = RINSE_TWICE_A_DAY

def set_throttle(i2c, current_cmd, ratio):
    reg_value = int(57 * ratio + 6)
    reg_value = reg_value << 2
    reg_value += current_cmd
    buf = bytes([reg_value])
    i2c.writeto_mem(I2C_MINIMOTO_ADDRESS, 0, buf)


def rinse(i2c, seconds):
    set_throttle(i2c, 1, 1)
    time.sleep(seconds)
    set_throttle(i2c, 0, 0)


def rinse_once():
    set_throttle(i2c_contoller, 1, 1)
    time.sleep(.5)
    set_throttle(i2c_contoller, 0, 0)


def run():
    start = pyb.millis()
    rinse_once()
    while True:
        if pyb.elapsed_millis(start) > RINSE_PERIOD:
            rinse_once()
            start = pyb.millis()

run()
