# Ant Rinser
## Description
This code is for PyBoard project for automatic ant rinsing. The PyBoard sends I2C signals to a MiniMoto driver to control a water minipump.
## Ingridients
![PyBoard](images/pyboard.jpg)

![MiniMoto driver](images/minimoto.jpg)

![Mini water pump](images/minipump.jpg)
## TODO
  - Add power check and signal for low power
  - Add moisture sensor to rinse only when needed, get rid of just schedule.
